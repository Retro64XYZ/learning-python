#!/usr/bin/python

# The modulo operator is %

a = 11 % 2
print(a)

# The exponential operator is **

b = 3 ** 40
print(b)

# Square Root

c = 16 ** (1/2)
print(c)

# Cube Root

d = 24 ** (1/3)
print(d)

# Combining Operations Using Complicated Expressions

e = (10 + 15) * 10

# Just like in High School, Python will follow the rules of PEMDAS
# Parentheses, exponents, multiplication, division, addition, and subtraction
