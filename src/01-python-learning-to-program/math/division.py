#!/usr/bin/python

a = 3 / 1
print(a)
b = 4 / 2
print(b)
c = 500 / 5
print(c)
d = 900 / 3.76
print(d)
# You can use the floor division operator to remove decimal values
e = 3 // 1
print(e)

